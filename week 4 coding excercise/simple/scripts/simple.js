function doIt()
{
    // Variables for HTML element DOM references.
    var num1Ref, num2Ref, num3Ref, answerRef, oddevenRef; 
    
    // Working variables.
    var num1, num2, num3, answer, oddeven; 
    
    // Get references to DOM elements.
    num1Ref = document.getElementById("number1");
    num2Ref = document.getElementById("number2");
    num3Ref = document.getElementById("number3");
    answerRef = document.getElementById("answer");
    oddevenRef = document.getElementById("oddeven");
        
    

    // Convert strings to numbers, e.g., "21" to 21.
    num1 = Number(num1Ref.value);
    num2 = Number(num2Ref.value);
    num3 = Number(num3Ref.value);
    
    // Perform addition operation then assignment operation
    answer = num1 + num2 + num3;

    // Update "answer" label DOM to show result.
    answerRef.innerText = answer;
    
    if (answer >= 0)
    {
        // Value of answer is positive.
        // Set the class of the answer label to "positive".
        answerRef.className = "positive";
    }
    else
    {
        // Value of answer is not positive, i.e., negative.
        // Set the class of the answer label to "negative".
        answerRef.className = "negative";
    }
    if (answer % 2 === 0)
    {
        // Value of answer is even.
        // Set the class of the answer label to "even".
        oddevenRef.innerText = "(even)"
        oddevenRef.className = "even";
    }
        else
    {
        // Value of answer is not even.
        // Set the class of the answer label to "odd".
        oddevenRef.innerText = "(odd)"
        oddevenRef.className = "odd";
    }
}
    