/*Using only the two literal Strings:
let animalString = "cameldogcatlizard";
let andString = " and ";
Use both String methods substr(…) and substring(…) to output “cat and dog”.
Look up documentation (w3schools or Mozilla) to understand the different semantics of the
parameters of these 2 methods. Be prepared to show your demonstrator the documentation you
used and to explain the difference between substr(…) and substring(…). */
let animalString = "cameldogcatlizard";
let andString = " and ";
// method 1 
//string.substring(start, end)
/*start	Required. The position where to start the extraction. First character is at index 0
end	Optional. The position (up to, but not including) where to end the extraction. If omitted, it extracts the rest of the string*/
d=animalString.substr(5,3)// extracting dog 
c=animalString.substr(8,3)// extracting cat
p="and"
console.log(d+" "+p+" "+c)
//method 2
//substring(a,b) a is the position of the first element to the position before b
d=animalString.substring(5,8)// extracting dog 
c=animalString.substring(8,11)//extracting cat
console.log(d+" "+p+" "+c)