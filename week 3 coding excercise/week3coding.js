//q1
let outputAreaRef1 = document.getElementById("outputArea1");
let output1 = "";
let output2 = "";
let output0 = "";

function objectToHTML(obj) {
    for (prop in obj) {
        output2 += prop + ": " + obj[prop] + "<br />";
        
    }
}

var testObj = {
 number: 1,
 string: "abc",
 array: [5, 4, 3, 2, 1],
 boolean: true
};
output1 += "BEFORE CALL:<br />" 
output1 += "number: " + testObj.number + "<br />string: " + testObj.string + "<br />array: " + testObj.array + "<br />boolean: " + testObj.boolean; 

objectToHTML(testObj);

outputAreaRef1.innerHTML = output2;

//q2
let outputAreaRef2 = document.getElementById("outputArea2");
let output3 = "";
let output4 = "";
let output5 = "";
let output7 = "";

function flexible(fOperation, operand1, operand2)
{
 var result = fOperation(operand1, operand2);
 return result;
}

function add(operand1, operand2) {
    output3 = operand1 + operand2;
    return output3;
}
let multiply = function (operand1,operand2) {
    output4 = operand1 * operand2;
    return output4;
}

output5 = flexible(add,3,5);
output7 = flexible(multiply,3,5);
outputAreaRef2.innerHTML = output5 + "<br />" + output7;

//q4,5
/*first we declare the array and pass it as an argument inside the fucntion extremesValues, by initiliazing two  variabless valmin and valmax and we pass the loop throught the whole array and we use an if statement to check the maximum value and and else if statement to to caculate the minimum value.  and then return the both values. */

let outputAreaRef3 = document.getElementById("outputArea3");
let output6 = "";
let values = [4, 3, 6, 12, 1, 3, 8];

function extremeValue(array) {
    let valmax = array[0];
    let valmin = array[0];
    for (let x=0; x<array.length; x++) {
        if (array[x] > valmax) {
            valmax = array[x];
        }
        else if (array[x] < valmin) {
            valmin = array[x];
        }
    }
    return valmax + "<br />" + valmin;
}

output6 = extremeValue(values)
outputAreaRef3.innerHTML = output6;